const mongoose = require("mongoose");
const drinkModel = require("../models/drinkModel");

const createDrink = (req, res) => {
    // 1: get data
    const {
        drinkCode,
        drinkName,
        price
    } = req.body;
    // console.log(drinkCode);
    // console.log(drinkName);
    // console.log(price);
    // 2: validate data
    if (drinkCode === undefined || drinkCode === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkCode is not valid"
        })
    }
    if (drinkName === undefined || drinkName === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkName is not valid"
        })
    }
    // negate "price != undefined or price is integer and greater or equal 0" proposition
    if (!(price !== undefined && (Number.isInteger(price) && price >= 0))) {
        return res.status(400).json({
            status: "Bad request",
            message: "price is not valid"
        })
    }
    // 3: process requirement
    // 3.1: prepare data
    var createDrink = {
        _id: mongoose.Types.ObjectId(),
        drinkCode,
        drinkName,
        price
    } 
    console.log(createDrink);
    // 3.2 call model to add data
    drinkModel.create(createDrink, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        return res.status(201).json({
            status: "Drink is created successfully",
            newDrink: data
        })
    })
}

const getAllDrink = (req, res) => {
    // 1: get data
    // 2: validate data
    // 3: process requirement
    // 3.1: prepare data
    // 3.2 call model to add data
    drinkModel.find()
    .select("drinkCode drinkName price")
    .exec((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        return res.status(200).json({
            drink: data
        })

    })
}

const getDrinkById = (req, res) => {
    // 1: get data
    const vDrinkId = req.params.drinkId;
    // 2: validate data
    if (!mongoose.Types.ObjectId.isValid(vDrinkId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Drink id is not valid"
        })
    }
    // 3: process requirement
    // 3.1: prepare data
    // 3.2 call model to add data
    drinkModel.findById(vDrinkId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        if (data) {
            return res.status(200).json({
                status: "Get drink successfully",
                drink: data
            })
        }
        return res.status(404).json({
            status: "Not found"
        })

    })
}

const updateDrinkById = (req, res) => {
    // 1: get data
    const {
        drinkCode,
        drinkName,
        price
    } = req.body;
    const vDrinkId = req.params.drinkId;
    // console.log(drinkCode);
    // console.log(drinkName);
    // console.log(price);
    // 2: validate data
    if (!mongoose.Types.ObjectId.isValid(vDrinkId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Drink id is not valid"
        })
    }
    if (drinkCode === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkCode is not valid"
        })
    }
    if (drinkName === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkName is not valid"
        })
    }
    // negate "price != undefined or price is integer and greater or equal 0" proposition
    if (price !== undefined && !(Number.isInteger(price) && price >= 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "price is not valid"
        })
    }
    // 3: process requirement
    // 3.1: prepare data
    const updateDrink = {};
    if (drinkCode) updateDrink.drinkCode = drinkCode;
    if (drinkName) updateDrink.drinkName = drinkName;
    if (price || price === 0) updateDrink.price = price;
    console.log(updateDrink);
    // 3.2 call model to add data
    drinkModel.findByIdAndUpdate(vDrinkId, updateDrink, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        if (data) {
            return res.status(201).json({
                status: "Drink is updated successfully",
                updateDrink: data
            })
        }
        return res.status(404).json({
            status: "Not found"
        })

    })
}

const deleteDrinkById = (req, res) => {
    // 1: get data
    const vDrinkId = req.params.drinkId;
    // 2: validate data
    if (!mongoose.Types.ObjectId.isValid(vDrinkId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Drink id is not valid"
        })
    }
    // 3: process requirement
    // 3.1: prepare data
    // 3.2 call model to add data
    drinkModel.findByIdAndDelete(vDrinkId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        if (data) {
            return res.status(204).json()
        }
        return res.status(404).json({
            status: "Not found"
        })

    })
}

module.exports = {
    createDrink,
    getAllDrink,
    getDrinkById,
    updateDrinkById,
    deleteDrinkById,
}