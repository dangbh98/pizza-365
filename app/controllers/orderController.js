// import mongoose library
const mongoose = require("mongoose");
// import rand-token
const randToken = require("rand-token");

// import model
const orderModel = require("../models/orderModel");
const userModel = require("../models/userModel");
const voucherModel = require("../models/voucherModel");
const drinkModel = require("../models/drinkModel");

// API functions
const createOrderOfUser = (req, res) => {
    // 1: get data
    const vUserId = req.params.userId;
    console.log(vUserId);
    const {
        pizzaSize,
        pizzaType,
        voucher,
        drink,
        status,
    } = req.body;

    console.log(req.body);
    // console.log(voucher);
    // 2: validate data
    if (pizzaSize === undefined || pizzaSize === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "pizzaSize is not valid"
        })
    }
    if (pizzaType === undefined || pizzaType === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "pizzaType is not valid"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(voucher)) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucher is not valid"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(drink)) {
        return res.status(400).json({
            status: "Bad request",
            message: "drink is not valid"
        })
    }
    if (status === undefined || status === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "status is not valid"
        })
    }
    // 3: process requirement
    // 3.1: prepare data
    var createOrderData = {
        _id: mongoose.Types.ObjectId(),
        orderCode: randToken.generate(12),
        pizzaSize,
        pizzaType,
        voucher,
        drink,
        status
    }
    // console.log(createDrink);
    // 3.2 call model to add data
    userModel.findById(vUserId, (err2, data2) => {
        if (err2) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err2.message
            })
        }
        if (data2) {
            orderModel.create(createOrderData, (err, data) => {
                if (err) {
                    return res.status(500).json({
                        status: "Internal sever error",
                        message: err.message
                    })
                }
                // add order id to array in User data
                userModel.findByIdAndUpdate(vUserId, {
                    $push: { order: data._id }
                }, (err1, data1) => {
                    if (err1) {
                        return res.status(500).json({
                            status: "Internal sever error",
                            message: err1.message
                        })
                    }
                    return res.status(201).json({
                        status: "Order is created successfully",
                        newOrder: data
                    })
                })
            })
        } else {
            return res.status(404).json({
                status: "Not found"
            })
        }
    })
}

const createOrder = (req, res) => {
    // 1: get data
    const {
        pizzaSize,
        diameter,
        bbqQuantity,
        salad,
        pizzaType,
        voucherCode,
        drinkCode,
        drinkQuantity,
        totalMoney,
        note,
        fullName,
        email,
        address,
        phone
    } = req.body;

    // console.log(req.body);
    // console.log(voucher);
    // 2: validate data
    if (pizzaSize === undefined || pizzaSize === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "pizzaSize is not valid"
        })
    }
    if (!(Number.isInteger(diameter) && diameter >= 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "diameter is not valid"
        })
    }
    if (!(Number.isInteger(bbqQuantity) && bbqQuantity >= 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "bbqQuantity is not valid"
        })
    }
    if (!(Number.isInteger(salad) && salad >= 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "salad is not valid"
        })
    }
    if (!(Number.isInteger(drinkQuantity) && drinkQuantity >= 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkQuantity is not valid"
        })
    }
    if (!(Number.isInteger(drinkQuantity) && drinkQuantity >= 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkQuantity is not valid"
        })
    }
    if (!(Number.isInteger(totalMoney) && totalMoney >= 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "totalMoney is not valid"
        })
    }
    if (pizzaType === undefined || pizzaType === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "pizzaType is not valid"
        })
    }
    if (voucherCode !== undefined && !voucherCode) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucherCode is not valid"
        })
    }
    if (!drinkCode) {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkCode is not valid"
        })
    }
    if (fullName === undefined || fullName === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "fullName is not valid"
        })
    }
    if (email === undefined || email === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "email is not valid"
        })
    }
    if (address === undefined || address === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "address is not valid"
        })
    }
    if (phone === undefined || phone === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is not valid"
        })
    }
    // 3: process requirement
    // create random orderCode
    let orderCode = randToken.generate(12);


    userModel.findOne({ email }, async (findUserErr, foundUserData) => {
        if (findUserErr) {
            return handleError(findUserErr, res);
        } else if (foundUserData) {
            // find voucher by order code
            var voucherData = await voucherModel.findOne({ voucherCode });
            var voucherId = undefined;
            if (voucherData) {
                voucherData = voucherData._doc;
                voucherId = voucherData._id
            }
            // console.log(voucherId);

            // find drink by drink code
            var drinkData = await drinkModel.findOne({ drinkCode });
            var drinkId = undefined;
            if (drinkData) {
                drinkData = drinkData._doc;
                drinkId = drinkData._id
            }
            // console.log(drinkId);
            // create order
            orderModel.create({
                _id: mongoose.Types.ObjectId(),
                pizzaSize,
                diameter,
                bbqQuantity,
                salad,
                pizzaType,
                voucher: voucherId,
                drink: drinkId,
                drinkQuantity,
                totalMoney,
                note,
                user: foundUserData._id
            }, (createOrderErr, createOrderData) => {
                if (createOrderErr) {
                    return handleError(createOrderErr, res);
                } else {
                    // update order to order array of user
                    userModel.findByIdAndUpdate(foundUserData._id, { $push: { order: createOrderData._id } }, (findUserByIdErr, foundUserByIdData) => {
                        if (findUserByIdErr) {
                            return handleError(findUserByIdErr, res);
                        }
                        return res.status(201).json({
                            status: "create order successfully",
                            order: createOrderData
                        })
                    })
                }
            })

        } else {
            userModel.create({
                _id: mongoose.Types.ObjectId(),
                fullName,
                email,
                address,
                phone
            }, async (createUserErr, createdUserData) => {
                if (createUserErr) {
                    return handleError(createUserErr, res);
                } else {
                    // find voucher by order code
                    var voucherData = await voucherModel.findOne({ voucherCode });
                    var voucherId = undefined;
                    if (voucherData) {
                        voucherData = voucherData._doc;
                        voucherId = voucherData._id
                    }
                    // console.log(voucherId);

                    // find drink by drink code
                    var drinkData = await drinkModel.findOne({ drinkCode });
                    var drinkId = undefined;
                    if (drinkData) {
                        drinkData = drinkData._doc;
                        drinkId = drinkData._id
                    }
                    // console.log(drinkId);
                    // create order
                    orderModel.create({
                        _id: mongoose.Types.ObjectId(),
                        pizzaSize,
                        diameter,
                        bbqQuantity,
                        salad,
                        pizzaType,
                        voucher: voucherId,
                        drink: drinkId,
                        drinkQuantity,
                        totalMoney,
                        note,
                        user: createdUserData._id
                    }, (createOrderErr, createOrderData) => {
                        if (createOrderErr) {
                            return handleError(createOrderErr, res);
                        } else {
                            userModel.findByIdAndUpdate(createdUserData._id, { $push: { order: createOrderData._id } }, (findUserByIdErr, foundUserByIdData) => {
                                if (findUserByIdErr) {
                                    return handleError(findUserByIdErr, res);
                                }
                                return res.status(201).json({
                                    status: "create order successfully",
                                    order: createOrderData
                                })
                            })
                        }
                    })
                }
            })
        }

    })
}

const getAllOrder = (req, res) => {
    // 1.Get data
    // 2.Validate Data
    // 3.Process requirement
    // 3.1 prepare data
    // 3.2 call model
    orderModel.find()
        .populate("voucher")
        .populate("drink")
        .populate("user")
        .exec((err, data) => {
            if (err) {
                res.status(500).json({
                    status: "Internal sever error",
                    message: err.message
                })
            }
            if (!data) {
                return res.status(404).json({
                    status: "Not found"
                })
            }
            return res.status(200).json({
                status: "Get all orders successfully",
                orders: data
            })
        })
}

const getOrderOfUser = (req, res) => {
    // 1.Get data
    const vUserId = req.params.userId;
    // 2.Validate Data
    if (!mongoose.Types.ObjectId.isValid(vUserId)) {
        return response.status(400).json({
            "status": "Bad Request",
            "Message": "userId is not valid!"
        })
    }
    // 3.Process requirement
    // 3.1 prepare data
    // 3.2 call model
    userModel.findById(vUserId)
        .populate("order")
        .exec((err, data) => {
            if (err) {
                res.status(500).json({
                    status: "Internal sever error",
                    message: err.message
                })
            }
            if (!data) {
                return res.status(404).json({
                    status: "Not found"
                })
            }
            return res.status(200).json({
                status: `Get all orders of userId ${vUserId} successfully`,
                orders: data
            })
        })
}

const getOrderByID = (req, res) => {
    // 1.Get data
    const vOrderId = req.params.orderId;
    // 2.Validate Data
    if (!mongoose.Types.ObjectId.isValid(vOrderId)) {
        return response.status(400).json({
            "status": "Bad Request",
            "Message": "orderId is not valid!"
        })
    }
    // 3.Process requirement
    // 3.1 prepare data
    // 3.2 call model
    orderModel.findById(vOrderId, (err, data) => {
        if (err) {
            res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        if (!data) {
            return res.status(404).json({
                status: "Not found"
            })
        }
        return res.status(200).json({
            status: `Get order successfully`,
            order: data
        })
    })
}

const updateOrderById = (req, res) => {
    // 1.Get data
    const vOrderId = req.params.orderId;
    const {
        pizzaSize,
        diameter,
        bbqQuantity,
        salad,
        pizzaType,
        voucherCode,
        drinkCode,
        drinkQuantity,
        totalMoney,
        note,
        fullName,
        email,
        address,
        phone
    } = req.body;
    // console.log(req.body);
    // 2.Validate Data
    if (pizzaSize === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "pizzaSize is not valid"
        })
    }
    if (diameter !== undefined && !(Number.isInteger(diameter) && diameter >= 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "diameter is not valid"
        })
    }
    if (bbqQuantity !== undefined && !(Number.isInteger(bbqQuantity) && bbqQuantity >= 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "bbqQuantity is not valid"
        })
    }
    if (salad !== undefined && !(Number.isInteger(salad) && salad >= 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "salad is not valid"
        })
    }
    if (drinkQuantity !== undefined && !(Number.isInteger(drinkQuantity) && drinkQuantity >= 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkQuantity is not valid"
        })
    }
    if (totalMoney !== undefined && !(Number.isInteger(totalMoney) && totalMoney >= 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "totalMoney is not valid"
        })
    }
    if (pizzaType === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "pizzaType is not valid"
        })
    }
    if (voucherCode !== undefined && !voucherCode) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucherCode is not valid"
        })
    }
    if (voucherCode !== undefined && !drinkCode) {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkCode is not valid"
        })
    }
    if (fullName === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "fullName is not valid"
        })
    }
    if (email === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "email is not valid"
        })
    }
    if (address === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "address is not valid"
        })
    }
    if (phone === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is not valid"
        })
    }
    // 3.Process requirement

    // 3.1 prepare data
    const updateOrderData = req.body;
    
    // 3.2 call model to add dat
    orderModel.findByIdAndUpdate(vOrderId, updateOrderData, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        if (!data) {
            return res.status(404).json({
                status: "Not found"
            })
        }
        return res.status(200).json({
            status: "Order is updated successfully",
            newOrder: data
        })
    })
}

const deleteOrderById = (req, res) => {
    // 1.Get data
    const vOrderId = req.params.orderId;
    const vUserId = req.params.userId;
    // 2.Validate Data
    if (!mongoose.Types.ObjectId.isValid(vOrderId)) {
        return response.status(400).json({
            "status": "Bad Request",
            "Message": "orderId is not valid!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(vUserId)) {
        return response.status(400).json({
            "status": "Bad Request",
            "Message": "orderId is not valid!"
        })
    }
    // 3.Process requirement
    // 3.1 prepare data
    // 3.2 call model
    userModel.findById(vUserId, (err, data) => {
        if (err) {
            res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        if (!data) {
            return res.status(404).json({
                status: "UserId not found"
            })
        }
        orderModel.findByIdAndDelete(vOrderId, (err1, data1) => {
            if (err1) {
                res.status(500).json({
                    status: "Internal sever error",
                    message: err1.message
                })
            }
            if (!data) {
                return res.status(404).json({
                    status: "OrderId not found"
                })
            }
            // remove order in array of user data
            userModel.findByIdAndUpdate(vUserId, { $pull: { order: vOrderId } }, (err2, data2) => {
                if (err2) {
                    return response.status(500).json({
                        status: "Internal server error",
                        message: err2.message
                    })
                }
                return res.status(204).json()
            })
        })
    })
}

const handleError = (paramError, paramResponse) => {
    return paramResponse.status(500).json({
        status: `Internal server error`,
        message: paramError.message
    })
}

const handleNotFound = (paramResponse) => {
    return paramResponse.status(404).json({
        status: "Not found"
    })
}

// export controller
module.exports = {
    createOrderOfUser,
    createOrder,
    getOrderOfUser,
    getOrderByID,
    getAllOrder,
    updateOrderById,
    deleteOrderById,
}