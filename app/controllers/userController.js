const mongoose = require("mongoose");
const userModel = require("../models/userModel");

const createUser = (req, res) => {
    // 1: get data
    const {
        fullName,
        email,
        address,
        phone,
        orders,
    } = req.body;
    // console.log(req.body);
    // 2: validate data
    if (fullName === undefined || fullName === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "fullName is not valid"
        })
    }
    if (email === undefined || email === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "email is not valid"
        })
    }
    if (address === undefined || address === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "address is not valid"
        })
    }
    if (phone === undefined || phone === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is not valid"
        })
    }
    // 3: process requirement
    // 3.1: prepare data
    var createUserData = {
        _id: mongoose.Types.ObjectId(),
        fullName,
        email,
        address,
        phone,
        orders,
    }
    // console.log(createUserData);
    // 3.2 call model to add data
    userModel.create(createUserData, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        return res.status(201).json({
            status: "User is created successfully",
            newUser: data
        })
    })
}

const getAllUser = (req, res) => {
    // 1: get data
    // 2: validate data
    // 3: process requirement
    // 3.1: prepare data
    // 3.2 call model to add data
    userModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: "Get all users successfully",
            users: data
        })

    })
}

const getAllUserLimit = (req, res) => {
    // 1: get data
    let limit = req.query.limit;
    // 2: validate data
    // 3: process requirement
    // 3.1: prepare data
    // 3.2 call model to add data
    userModel.find()
    .limit(limit)
    .exec((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: "Get all users successfully",
            users: data
        })

    })
}

const getAllUserSkip = (req, res) => {
    // 1: get data
    let skip = req.query.skip;
    // 2: validate data
    // 3: process requirement
    // 3.1: prepare data
    // 3.2 call model to add data
    userModel.find()
    .skip(skip)
    .exec((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: "Get all users successfully",
            users: data
        })

    })
}

const getAllUserSort = (req, res) => {
    // 1: get data
    // 2: validate data
    // 3: process requirement
    // 3.1: prepare data
    // 3.2 call model to add data
    userModel.find()
    .sort({fullName:"asc"})
    .exec((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: "Get all users successfully",
            users: data
        })

    })
}

const getUserSkipLimit = (req, res) => {
    // 1: get data
    let skip = req.query.skip;
    // 2: validate data
    // 3: process requirement
    // 3.1: prepare data
    // 3.2 call model to add data
    userModel.find()
    .skip(skip)
    .limit(1)
    .exec((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: "Get all users successfully",
            users: data
        })

    })
}

const getUserSortSkipLimit = (req, res) => {
    // 1: get data
    let skip = req.query.skip;
    // 2: validate data
    // 3: process requirement
    // 3.1: prepare data
    // 3.2 call model to add data
    userModel.find()
    .sort({fullName: "asc"})
    .skip(skip)
    .limit(1)
    .exec((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: "Get all users successfully",
            users: data
        })

    })
}

const getUserById = (req, res) => {
    // 1: get data
    const vUserId = req.params.userId;
    // 2: validate data
    if (!mongoose.Types.ObjectId.isValid(vUserId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "UserId is not valid"
        })
    }
    // 3: process requirement
    // 3.1: prepare data
    // 3.2 call model to add data
    userModel.findById(vUserId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        if (data) {
            return res.status(200).json({
                status: "Get user successfully",
                user: data
            })
        }
        return res.status(404).json({
            status: "Not found"
        })

    })
}

const updateUserById = (req, res) => {
    // 1: get data
    const {
        fullName,
        email,
        address,
        phone,
        orders,
    } = req.body;
    const vUserId = req.params.userId;
    // 2: validate data
    if (!mongoose.Types.ObjectId.isValid(vUserId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Drink id is not valid"
        })
    }
    if (fullName === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "fullName is not valid"
        })
    }
    if (email === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "email is not valid"
        })
    }
    if (address === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "address is not valid"
        })
    }
    if (phone === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is not valid"
        })
    }
    // 3: process requirement
    // 3.1: prepare data
    const updateUserData = {};
    if (fullName) updateUserData.fullName = fullName;
    if (email) updateUserData.email = email;
    if (address) updateUserData.address = address;
    if (phone) updateUserData.email = phone;
    console.log(updateUserData);
    // 3.2 call model to add data
    userModel.findByIdAndUpdate(vUserId, updateUserData, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        if (data) {
            return res.status(200).json({
                status: "Update user successfully",
                user: data
            })
        }
        return res.status(404).json({
            status: "Not found"
        })
    })
}

const deleteUserById = (req, res) => {
    // 1: get data
    const vUserId = req.params.userId;
    // 2: validate data
    if (!mongoose.Types.ObjectId.isValid(vUserId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "User id is not valid"
        })
    }
    // 3: process requirement
    // 3.1: prepare data
    // 3.2 call model to add data
    userModel.findByIdAndDelete(vUserId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        if (data) {
            return res.status(204).json()
        }
        return res.status(404).json({
            status: "Not found"
        })

    })
}

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById,
    getAllUserLimit,
    getAllUserSkip,
    getAllUserSort,
    getUserSkipLimit,
    getUserSortSkipLimit
}