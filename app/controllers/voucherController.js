const mongoose = require("mongoose");
const voucherModel = require("../models/voucherModel");

const createVoucher = (req, res) => {
    // 1: get data
    const {
        voucherCode,
        discountPercent,
        note
    } = req.body;
    // console.log(voucherCode);
    // console.log(tenNuocUong);
    // console.log(note);
    // 2: validate data
    if (voucherCode === undefined || voucherCode === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "voucherCode is not valid"
        })
    }
    if (!(discountPercent !== undefined && (Number.isInteger(discountPercent) && discountPercent >= 0))) {
        return res.status(400).json({
            status: "Bad request",
            message: "discountPercent is not valid"
        })
    }
    // 3: process requirement
    // 3.1: prepare data
    var createVoucher = {
        _id: mongoose.Types.ObjectId(),
        voucherCode,
        discountPercent,
        note,
    }
    console.log(createVoucher);
    // 3.2 call model to add data
    voucherModel.create(createVoucher, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        return res.status(201).json({
            status: "Voucher is created successfully",
            newVoucher: data
        })
    })
}

const getAllVoucher = (req, res) => {
    // 1: get data
    // 2: validate data
    // 3: process requirement
    // 3.1: prepare data
    // 3.2 call model to add data
    voucherModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: "Get all vouchers successfully",
            vouchers: data
        })

    })
}

const getVoucherByVoucherCode = (req, res) => {
    // 1: get data
    const vVoucherCode = req.params.voucherCode;
    // 2: validate data
    if (!vVoucherCode) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucherCode id is not valid"
        })
    }
    // 3: process requirement
    // 3.1: prepare data
    // 3.2 call model to add data
    voucherModel.findOne({ voucherCode: vVoucherCode }, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        if (!data) {
            return res.status(404).json({
                status: "Not found"
            })
        }
        return res.status(200).json({
            status: "Get voucher successfully",
            voucher: data
        })

    })
}

const getVoucherById = (req, res) => {
    // 1: get data
    const vVoucherId = req.params.voucherId;
    // 2: validate data
    if (!mongoose.Types.ObjectId.isValid(vVoucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Voucher id is not valid"
        })
    }
    // 3: process requirement
    // 3.1: prepare data
    // 3.2 call model to add data
    voucherModel.findById(vVoucherId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        if (data) {
            return res.status(200).json({
                status: "Get voucher successfully",
                voucher: data
            })
        }
        return res.status(404).json({
            status: "Not found"
        })

    })
}

const updateVoucherById = (req, res) => {
    // 1: get data
    const {
        voucherCode,
        discountPercent,
        note
    } = req.body;
    const vVoucherId = req.params.voucherId;
    // console.log(voucherCode);
    // console.log(tenNuocUong);
    // console.log(note);
    // 2: validate data
    if (!mongoose.Types.ObjectId.isValid(vVoucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Voucher id is not valid"
        })
    }
    if (voucherCode === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "voucherCode is not valid"
        })
    }
    if (discountPercent === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "discountPercent is not valid"
        })
    }
    // 3: process requirement
    // 3.1: prepare data
    const updateDrink = {};
    if (voucherCode) updateDrink.voucherCode = voucherCode;
    if (discountPercent) updateDrink.discountPercent = discountPercent;
    if (note) updateDrink.note = note;
    console.log(updateDrink);
    // 3.2 call model to add data
    voucherModel.findByIdAndUpdate(vVoucherId, updateDrink, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        if (data) {
            return res.status(201).json({
                status: "Voucher is updated successfully",
                updatedVoucher: data
            })
        }
        return res.status(404).json({
            status: "Not Found"
        })
    })
}

const deleteVoucherById = (req, res) => {
    // 1: get data
    const vVoucherId = req.params.voucherId;
    // 2: validate data
    if (!mongoose.Types.ObjectId.isValid(vVoucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Voucher id is not valid"
        })
    }
    // 3: process requirement
    // 3.1: prepare data
    // 3.2 call model to add data
    voucherModel.findByIdAndDelete(vVoucherId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal sever error",
                message: err.message
            })
        }
        if (data) {
            return res.status(204).json()
        }
        return res.status(404).json({
            status: "Not found"
        })

    })
}

module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById,
    getVoucherByVoucherCode,
}