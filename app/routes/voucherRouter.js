// import express library
const express = require("express");
const router = express.Router();

// import router middleware 
const voucherRouterMiddleware = require("../middlewares/voucherMiddleware");

// import controllers
const voucherController = require("../controllers/voucherController");

router.get('/vouchers', voucherRouterMiddleware.getAllVoucherMiddleware, voucherController.getAllVoucher)

router.get('/voucher-detail/:voucherCode', voucherRouterMiddleware.getVoucherDetailMiddleware, voucherController.getVoucherByVoucherCode)

router.get('/vouchers/:voucherId', voucherRouterMiddleware.getVoucherDetailMiddleware, voucherController.getVoucherById)

router.post('/vouchers', voucherRouterMiddleware.createVoucherMiddleware, voucherController.createVoucher)

router.put('/vouchers/:voucherId', voucherRouterMiddleware.updateVoucherMiddleware, voucherController.updateVoucherById)

router.delete('/vouchers/:voucherId', voucherRouterMiddleware.deleteVoucherMiddleware, voucherController.deleteVoucherById)


// Export router thành 1 module
module.exports = router;

