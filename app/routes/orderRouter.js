// import express library
const express = require("express");
const router = express.Router();

// import router middleware 
const orderRouterMiddleware = require("../middlewares/orderMiddleware");

// import controller
const orderController = require("../controllers/orderController");

router.get('/orders', orderRouterMiddleware.getAllOrderMiddleware, orderController.getAllOrder)

router.post('/orders', orderRouterMiddleware.createOrderMiddleware, orderController.createOrder)

router.get('/orders/:orderId', orderRouterMiddleware.getOrderDetailMiddleware, orderController.getOrderByID)

router.get('/users/:userId/orders', orderRouterMiddleware.getAllOrderMiddleware, orderController.getOrderOfUser)

router.post('/users/:userId/orders', orderRouterMiddleware.createOrderMiddleware, orderController.createOrderOfUser)

router.put('/orders/:orderId', orderRouterMiddleware.updateOrderMiddleware, orderController.updateOrderById)

router.delete('/users/:userId/orders/:orderId', orderRouterMiddleware.deleteOrderMiddleware, orderController.deleteOrderById)


// Export router thành 1 module
module.exports = router;

