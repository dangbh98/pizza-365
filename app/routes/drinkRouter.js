// import express library
const express = require("express");

const router = express.Router();

// import router middleware
const drinkMiddleware = require("../middlewares/drinkMiddlewares");

// import controller
const drinkController = require("../controllers/drinkController");


router.get('/drinks', drinkMiddleware.getAllDrinksMiddleware, drinkController.getAllDrink)

router.get('/drinks/:drinkId', drinkMiddleware.getDrinkDetailMiddleware, drinkController.getDrinkById)

router.post('/drinks', drinkMiddleware.createDrinkMiddleware, drinkController.createDrink)

router.put('/drinks/:drinkId', drinkMiddleware.updateDrinkMiddleware, drinkController.updateDrinkById)

router.delete('/drinks/:drinkId', drinkMiddleware.deleteDrinkMiddleware, drinkController.deleteDrinkById)


// Export router thành 1 module
module.exports = router;