// import express library
const express = require("express");

const router = express.Router();

// import middle ware
const userMiddleware = require("../middlewares/userMiddleware");

//import controller
const userController = require("../controllers/userController");

router.get("/users", userMiddleware.getAllUserMiddleware,userController.getAllUser)

router.get("/limit-users", userMiddleware.getAllUserMiddleware,userController.getAllUserLimit)

router.get("/skip-users", userMiddleware.getAllUserMiddleware,userController.getAllUserSkip)

router.get("/sort-users", userMiddleware.getAllUserMiddleware,userController.getAllUserSort)

router.get("/skip-limit-users", userMiddleware.getAllUserMiddleware,userController.getUserSkipLimit)

router.get("/sort-skip-limit-users", userMiddleware.getAllUserMiddleware,userController.getUserSortSkipLimit)

router.get("/users/:userId", userMiddleware.getUserDetailMiddleware,userController.getUserById)

router.post("/users", userMiddleware.createUserMiddleware,userController.createUser)

router.put("/users/:userId", userMiddleware.updateUserMiddleware,userController.updateUserById)

router.delete("/users/:userId", userMiddleware.deleteUserMiddleware,userController.deleteUserById)


// export router
module.exports = router;
