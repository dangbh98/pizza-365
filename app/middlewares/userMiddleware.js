const userRouterMiddleware = (req, res, next) => {
    console.log(`User middleware url: ${req.url}`); 
    next();
};

const getAllUserMiddleware = (req, res, next) => {
    console.log(`Get all user middle ware`);
    next()
}

const getUserDetailMiddleware = (req, res, next) => {
    console.log(`Get user detail middle ware`);
    next()
}
const createUserMiddleware = (req, res, next) => {
    console.log(`Create user middle ware`);
    next()
}
const updateUserMiddleware = (req, res, next) => {
    console.log(`Update user middle ware`);
    next()
}

const deleteUserMiddleware = (req, res, next) => {
    console.log(`Delete user middle ware`);
    next()
}
module.exports = {
    userRouterMiddleware,
    getAllUserMiddleware,
    getUserDetailMiddleware,
    createUserMiddleware,
    updateUserMiddleware,
    deleteUserMiddleware
}