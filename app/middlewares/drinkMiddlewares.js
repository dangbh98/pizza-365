// drink middleware
const drinkRouterMiddleware = (req, res, next) => {
    console.log("Drinks URL: ", req.url);
    
    next();
}


const getAllDrinksMiddleware = (req, res, next) => {
    console.log("Get ALL Drink Middleware");
    
    next();
}

const getDrinkDetailMiddleware = (req, res, next) => {
    console.log("Get Drink Detail Middleware");
    
    next();
}

const createDrinkMiddleware = (req, res, next) => {
    console.log("Create Drink Middleware");
    
    next();
}

const updateDrinkMiddleware = (req, res, next) => {
    console.log("Update Drink Middleware");
    
    next();
}

const deleteDrinkMiddleware = (req, res, next) => {
    console.log("Delete Drink Middleware");
    
    next();
}
module.exports = {
    drinkRouterMiddleware,
    getAllDrinksMiddleware,
    getDrinkDetailMiddleware,
    createDrinkMiddleware,
    updateDrinkMiddleware,
    deleteDrinkMiddleware
}