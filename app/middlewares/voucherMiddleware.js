const voucherRouterMiddleware = (req, res, next) => {
    console.log(`Voucher Url ${req.url}`);
    next();
}

const getAllVoucherMiddleware = (req, res, next) => {
    console.log(`Get all voucher middleware`);
    next();
}

const getVoucherDetailMiddleware = (req, res, next) => {
    console.log(`Get voucher detail middleware`);
    next();
}

const createVoucherMiddleware = (req, res, next) => {
    console.log(`Create voucher middleware`);
    next();
}

const updateVoucherMiddleware = (req, res, next) => {
    console.log(`Update voucher middleware`);
    next();
}

const deleteVoucherMiddleware = (req, res, next) => {
    console.log(`Delete voucher middleware`);
    next();
}


module.exports = {
    voucherRouterMiddleware,
    getAllVoucherMiddleware,
    getVoucherDetailMiddleware,
    createVoucherMiddleware,
    updateVoucherMiddleware,
    deleteVoucherMiddleware
}
