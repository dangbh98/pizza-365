const orderRouterMiddleware = (req, res, next) => {
    console.log(`Order Url ${req.url}`);
    next();
}

const getAllOrderMiddleware = (req, res, next) => {
    console.log(`Get all order middleware`);
    next();
}

const getOrderDetailMiddleware = (req, res, next) => {
    console.log(`Get order detail middleware`);
    next();
}

const createOrderMiddleware = (req, res, next) => {
    console.log(`Create order middleware`);
    next();
}

const updateOrderMiddleware = (req, res, next) => {
    console.log(`Update order middleware`);
    next();
}

const deleteOrderMiddleware = (req, res, next) => {
    console.log(`Delete order middleware`);
    next();
}


module.exports = {
    orderRouterMiddleware,
    getAllOrderMiddleware,
    getOrderDetailMiddleware,
    createOrderMiddleware,
    updateOrderMiddleware,
    deleteOrderMiddleware
}
