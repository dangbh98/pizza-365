// import mongoose library
const mongoose = require("mongoose");

// import schema
const Schema = mongoose.Schema;

// create schema
const drinkSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    drinkCode: {
        type: String,
        unique: true,
        require: true
    },
    drinkName: {
        type: String,
        require: true
    },
    price: {
        type: Number,
        require: true
    }
})

module.exports = mongoose.model('drink', drinkSchema);