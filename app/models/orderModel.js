// import mongoose library
const mongoose = require("mongoose");
const { stringify } = require("qs");
const randToken = require("rand-token")
// import schema
const Schema = mongoose.Schema;

// create schema
const orderSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    orderCode: {
        type: String,
        unique: true,
        default: () => {
            return randToken.generate(12);
        }
    },
    pizzaSize: {
        type: String,
        require: true
    },
    diameter: {
        type: Number,
        require: true
    },
    bbqQuantity: {
        type: Number,
        require: true
    },
    salad: {
        type: Number,
        require: true
    },
    pizzaType: {
        type: String,
        require: true
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "voucher"
    },
    drink: {
        type: mongoose.Types.ObjectId,
        ref: "drink"
    },
    drinkQuantity: {
        type: Number,
        require: true
    },
    totalMoney: {
        type: Number,
        require: true
    },
    note: {
        type: String,
        require: false
    },
    user: {
        type: mongoose.Types.ObjectId,
        ref: "user"
    }, 
    status:{
        type: String,
        default: "Open"
    }
},
{timestamps: {
    createdAt: 'createAt',
    updatedAt: 'updateAt'
}}
)

module.exports = mongoose.model('order', orderSchema);