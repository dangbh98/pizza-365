// import mongoose library
const mongoose = require("mongoose");

// import schema
const Schema = mongoose.Schema;

// create schema
const userSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        require: true
    },
    email: {
        type: String,
        unique: true,
        require: true
    },
    address: {
        type: String,
        require: true
    },
    phone: {
        type: String,
        unique: true,
        require: true
    },
    order: [{ type: mongoose.Types.ObjectId, ref: "order" }]
}) 

module.exports = mongoose.model('user', userSchema);