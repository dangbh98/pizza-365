// import mongoose library
const mongoose = require("mongoose");

// import schema
const Schema = mongoose.Schema;

// create schema
const voucherSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    voucherCode: {
        type: String,
        unique: true,
        require: true
    },
    discountPercent: {
        type: Number,
        require: true
    },
    note: {
        type: String,
        require: false
    },
})

module.exports = mongoose.model('voucher', voucherSchema);