// import express library
const express = require("express");
// import mongoose library
const mongoose = require("mongoose");

//import router
const drinkRouter = require("./app/routes/drinkRouter");
const voucherRouter = require("./app/routes/voucherRouter");
const orderRouter = require("./app/routes/orderRouter");
const userRouter = require("./app/routes/userRouter");

const path = require("path");

const app = express();

const port = 8000;

// connect with mongoose
mongoose.connect('mongodb://127.0.0.1:27017/CRUD_Pizza365',(error) => {
    if(error) throw error;
    console.log("Successfully connected");
})

// Config app to get body json data
app.use(express.json());

// config app to display image on html page
app.use(express.static(__dirname + "/views"))

// send file html display to user
app.get("/",(req, res) => {
    res.sendFile(path.join(__dirname + `/views/index.html`))
})
app.get("/order-list",(req, res) => {
    res.sendFile(path.join(__dirname + `/views/orderList.html`))
})

// config api for drink router
app.use("/devcamp-pizza365", drinkRouter);
// config api for voucher router
app.use("/devcamp-pizza365", voucherRouter);
// config api for order router
app.use("/devcamp-pizza365", orderRouter);
// config api for user router
app.use("/devcamp-pizza365", userRouter);


// running app in port 
app.listen(port, () => {
    console.log(`App is listening in port ${port}`);
})